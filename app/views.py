from django.shortcuts import render
from .models import ClipBoard
from django.http import Http404
from django.views.generic import ListView
# Create your views here.
class HomeView(ListView):
    model=ClipBoard
    template_name='index.html'
    paginate_by=5
    paginate_orphans=1
        
    def paginate_queryset(self,queryset,page_size):
        try:
            return super(HomeView,self).paginate_queryset(queryset,page_size)
        except Http404:
            self.kwargs['page']=1
            return super(HomeView,self).paginate_queryset(queryset,page_size)
